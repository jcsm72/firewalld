Configuracion de firewalld 
=========================

Esta guía presentará FirewallD, sus nociones con respecto a las zonas y servicios y le mostrará los pasos básicos de configuración tiene como objetivo no hacer la vista gorda y simplemente hacer un ::

   systemctl disable firewalld.service


Conjuntos de configuración
++++++++++++++++++++++++

Firewalld utiliza dos conjuntos de configuración: en ejecución y permanente. Los cambios en la configuración en ejecución no son retenidos tras un reinicio del servidor o después de reiniciar FirewallD, mientras que los cambios permanentes no son aplicados a un sistema en ejecución.

Por defecto, los comandos firewall-cmd aplicados a la configuración en ejecución, pero que usan la bandera --permanent establecerán una configuración persistente. Para agregar y activar una regla permanente puede usar uno de estos dos métodos:

Agregar la regla al conjunto de configuración en ejecución. Por ejemplo: ::

   sudo firewall-cmd --zone=public --add-service=http


Añadir la regla al conjunto permanente y volver a cargar FirewallD. Por ejemplo: ::

   sudo firewall-cmd --zone=public --add-service=http --permanent
   sudo firewall-cmd --reload

Nota: El comando de recarga elimina cualquier configuración en ejecución y aplica la configuración permanente almacenada. Debido a que FirewallD administra el conjunto de reglas de forma dinámica, no se interrumpirán las conexiones y sesiones existentes.

Trabajar con servicios o puerto/protocolo arbitrario
+++++++++++++++++++++++++++++++++++++++++++++++++++

FirewallD puede admitir tráfico con base en reglas predefinidas según servicios de red específicos. Puede crear sus propias reglas personalizadas para un servicio y agregarlas a cualquier zona.


Para ver los servicios disponibles de forma predeterminada: ::

   sudo firewall-cmd --get-services

Por ejemplo, para habilitar o deshabilitar el servicio HTTP: ::

   sudo firewall-cmd --zone=public --add-service=http --permanent
   sudo firewall-cmd --zone=public --remove-service=http --permanent

Digamos que queremos permitir o denegar el tráfico TCP en el puerto 12345. Podríamos usar el siguiente comando: ::

   sudo firewall-cmd --zone=public --add-port=12345/tcp --permanent
   sudo firewall-cmd --zone=public --remove-port=12345/tcp --permanent


Zonas de Firewalld
++++++++++++++++

Las zonas son conjuntos de reglas predefinidas para varios niveles de confianza que probablemente utilizaría en ubicaciones o escenarios comunes (ejemplos: en el hogar, en una red pública, en una red de confianza, etc). Las distintas zonas admiten distintos servicios de red y tipos de tráfico entrante mientras que niegan todo lo demás. Después de activar FirewallD por primera vez, Public o "pública" será su zona predeterminada.

Las zonas también pueden ser aplicadas a diferentes interfaces de red. Por ejemplo, con interfaces separadas tanto para una red interna como para el Internet, puede permitir DHCP en una zona interna pero solo HTTP y SSH en una zona externa. Cualquier interfaz que no esté establecida explícitamente en una zona especificada será añadida a la zona predeterminada.

Para ver la zona predeterminada ejecute: ::

   sudo firewall-cmd --get-default-zone

Para cambiar la zona predeterminada puede utilizar: ::

   sudo firewall-cmd --set-default-zone=internal

Para ver las zonas utilizadas por su(s) interfaz o interfaces de red: ::

   sudo firewall-cmd --get-active-zones

Ejemplo de una salida: ::

   public
      interfaces: eth0

Para obtener todas las configuraciones para una zona específica: ::

   sudo firewall-cmd --zone=public --list-all

Ejemplo de salida: ::

   public (default, active)
      interfaces: ens160
      sources:
      services: dhcpv6-client http ssh
      ports: 12345/tcp
      masquerade: no
      forward-ports:
      icmp-blocks:
      rich rules:


Para obtener todas las configuraciones para todas las zonas: ::

   sudo firewall-cmd --list-all-zones

Salida de ejemplo: ::

   block
      interfaces:
      sources:
      services:
      ports:
      masquerade: no
      forward-ports:
      icmp-blocks:
      rich rules:

      ...

   work
      interfaces:

      sources:
      services: dhcpv6-client ipp-client ssh
      ports:
      masquerade: no
      forward-ports:
      icmp-blocks:
      rich rules:

`Documentacion Original <docs.bluehosting.cl/tutoriales/servidores/introduccion-a-firewalld-en-centos.html>`_.
